
\chapter{Background} \label{chap:bckg}
This chapter presents the background of this work, with a quick review of the \gls{CPU} cache. This review starts from the reasons that caused its adoption (\autoref{sec:motivs}), then shows its structure (\autoref{sec:cache_struct}) and explains how more caches are organized into a hierarchy inside a \gls{CPU} (\autoref{sec:cache_hier}). Then, we present a basic explanation of contention phenomena in \autoref{sec:contention}.

We also present the main algorithm for page allocation in \autoref{sec:buddy}.

Finally, in \autoref{sec:hugepages} we explain how modern \glspl{CMP} can manage physical memory areas with a different granularities.


\section{Motivations of caching} \label{sec:motivs}
From the 80's, the architectural and technological evolutions of \glspl{CPU} and \glspl{RAM} caused a diverging growth of the speed of these two components. Their speeds grew up at an exponential pace, but the coefficient of growth is different in favor of the \gls{CPU} speed; therefore, the difference between the two speeds also grew up exponentially, thus creating over the years the so-called \emph{processor-memory gap} \cite{carvalho2002gap}. This gap forces the \gls{CPU} to wait for the completion of the memory operations and may dramatically decrease the effective performance of the \gls{CPU}, causing its \gls{IPC} to be much less than the expected one. This happens because the \gls{CPU} can spend most of its time waiting for the data. Hence, the need of overcoming this bottleneck arose and a faster memory was needed.

To build such memories, designers leverage a key feature of applications that is the principle of \emph{access locality}, which is distinguished in \emph{spatial locality} and \emph{temporal locality}.

\begin{defn}
(Spatial locality) \itshape if a (virtual) memory address $ n $ is referenced, it is likely that the address $ n + 1 $ will be referenced in the near future.
\end{defn}

\begin{defn}
(Temporal locality) \itshape if a memory address is referenced at cycle $ c $, it is likely that it will be referenced again at cycle $ c + T $ (with $ T $ small).
\end{defn}

By combining these two principles, it turns out that if a block of words is referenced by the \gls{CPU},  it is likely to be referenced again and multiple times in the near future. This intuition, proved by experimental evidence from the execution traces of many programs, suggested the introduction of a \emph{\gls{CPU} cache}, i.e. a fast memory that keeps the most used data ``close'' to the \gls{CPU}.

Typically, caches are implemented with \gls{SRAM} technology. This technology has a greater cost in terms of area and energy with respect to the cost of main memory (typically implemented with \gls{DRAM} technology), but permits to build memories whose access time is much smaller, thus ensuring more performance. For these reasons, the size of cache memories is a careful trade-off that takes into account many parameters.


\section{Structure of cache} \label{sec:cache_struct}
The basic granularity for data management inside the cache is the \emph{line} (also called \emph{block}, with possible confusion with the granularity of main memory). A cache loads (or discards) the data in units of lines from main memory, where a cache line has a size that is bigger than a word and always a power of 2: in symbols the size is $ L = 2^{l} $. For example, the line size in the x86 architecture is 64 B (with word of 32 or 64 bits), while in the Power architecture it is 128 B.

A line always stores contiguous words from main memory. Using cache lines bigger than the \gls{CPU} word permits to leverage spatial locality, because the cache stores also the words that are adjacent to the requested one. Instead, a more complex mechanism leverages temporal locality; this mechanism determines most of the features of the cache, and is explained in \autoref{subsec:cache_design}.\\
Since \glspl{CPU} operate in virtual mode, the choice of which address space to use to address data for caching is a fundamental design choice: \autoref{subsec:cache_addr} explains the advantages and drawbacks of each choice.\\
Finally, \autoref{cache_others} shows other types of caches that can be found on the market.

\subsection{Design, operations and types of caches} \label{subsec:cache_design}
Based on the trade-off between latency and performance, several cache models exist to leverage temporal locality. The most general one is the \emph{$ n $-way set-associative cache}, where $ n $ is usually an even number. The whole cache is divided into \emph{sets}, each set containing $ n $ lines. The number of sets in a cache is $ 2^{s} $, where $s$ is the number of bits used to address the cache set. Using both line size and set number in powers of two makes it possible to use subsets of the data address bits to address the cache. In particular, the less significant $ l $ bits determine the offset inside the line of the referenced byte (they are called \emph{line offset} bits), while the $ s $ bits after the line offset are the \emph{set number} bits and determine the cache set. This hint avoids expensive computations of cache locations and is realizable simply by re-routing the address wires. 

If we assume a set to be initially empty, the first $ n $ lines are loaded in the free positions. To check whether a referenced line is already present in cache, the most significant bits of the data address are used: these bits are the \emph{tag bits} (see \autoref{fig:addr_cache}), and are used as a label to mark each line in the set. When a word is referenced, its set is obtained from the address and its tag is compared to the tags of all the lines inside the set. This comparison is done in parallel (thus justifying the name of ``associative cache'') and is the most expensive operation in terms of latency; the other major component in latency is the time to access the set. When a line is found, a \emph{cache hit} happens, otherwise a \emph{cache miss} happens.

\begin{figure}
\centering
\includegraphics[width=.8\textwidth]{img/addr_cache.eps}
\caption{Bit fields of a memory address to access a cache}
The physical address is divided into three fields for cache access: line offset, set number and tag.
\label{fig:addr_cache}
\end{figure}

How sets are managed when they are full is they key mechanism to leverage temporal locality.
In fact, when the $ n + 1 $-th line is inserted, the cache must discard (or \emph{evict}) a line and load the incoming one in its place. The line to be evicted is chosen usually according to the \gls{LRU} policy. Other policies have been employed, like random, \gls{FIFO} and \gls{LIFO}, but are uncommon in modern architectures. The \gls{LRU} policy prescribes to evict the line that is the least recently used one. Thus, after the eviction, the set contains the new line and the $ n - 1 $ most used lines.

The \gls{LRU} policy is implemented by adding to each cache line a field with a counter storing the number of accesses: when a line is loaded its counter is set to 0, when the line is referenced its counter is increased. If a counter overflows, it is typically left to the maximum value and all the others counters inside the set are decremented.

The choice of the parameters of the cache is a trade-off between different objectives: in principle, the number $n$ of ways per set (the so-called \emph{associativity}) must be the maximum possible, in order to leverage temporal locality. But a higher associativity implies a longer time for line lookup during each cache access (and this operation is on the critical path) and more area to implement the comparators. Therefore, the associativity is in practice limited to a small set of value, usually ranging from 2 to 16.

At the extreme positions of this model, there are \emph{fully associative} caches and \emph{direct-mapped} caches. A fully associative cache can be viewed as a particular case of an $ n $-way set-associative cache where $n$ is the maximum possible number given a cache size $ S $, i.e. $ n = \frac{S}{L} $: in this model the whole cache has only one set and the highest possible associativity. This model thus applies the \gls{LRU} policy to the whole cache, and exploits temporal locality at best; but, because of the limitations mentioned above, this type is rare in practice. Instead, in direct-mapped caches each set contains only one line ($n=1$) and the number of sets is the maximum possible, i.e. $ \frac{S}{L} $. This model does not provide the benefits of temporal locality, but is easy to implement as the cache line coincides with the set and can be determined just by the set bits, without any comparator or counter, thus with a minimum latency. Caches of this type were used in the past years for small, fast caches at the lowest levels of the memory hierarchy (see \autoref{sec:cache_hier}).


\subsection{Cache addressing} \label{subsec:cache_addr}
During the cache access, two fields are used for the line look up: the set number and the line tag. Both these fields are taken from the memory address of the referenced word. Modern \glspl{CPU} provide two address spaces for data, the virtual address and the physical address. The physical address is the one used by the \gls{CPU} to load data from \gls{RAM}. The physical address space is unique and is usually managed by the \gls{OS} only. Instead, applications use virtual addresses to reference memory, and cannot use physical addresses. Each application has a separate virtual address space, which is mapped to the physical address by the \gls{OS} in a way that is transparent to applications. Therefore, the same virtual address in the context of two applications can reference different physical locations in \gls{RAM}. The translation from a referenced virtual address to the corresponding physical one is performed at runtime by the \gls{TLB}; this component resides in the \gls{CPU} and its performance is fundamental.

Both address spaces can be used for cache addressing, with advantages and drawbacks. Moreover, as the set number and the tag are needed for different purposes, one address space can be used for the set and the other for the tag.

A cache can be \gls{PIPT}, with the advantage that both the set number and the tag are unique for all the addresses referenced, as all the addresses come from the same address space. The drawback of this model is that the \gls{TLB} translation is needed before accessing the cache, thus increasing the latency. However, most of the modern caches are implemented in this way.

Instead, a \gls{VIVT} cache uses the virtual address for both the set number and the tag, with no latency penalty. Caches of this type suffer from two main problems, collectively called \emph{aliasing}. In case a used virtual address is mapped to another physical address, the data in cache become inconsistent (\emph{homonyms problem}). Mapping the same virtual address to different physical ones happens very frequently, as each application has its own virtual address space. Conversely, different virtual addresses can refer to the same physical locations (for example to share data, as with inter-process communication mechanisms), thus creating potentially non-coherent copies of the same data with also wasting space. Possible solutions to overcome these problems are flushing the whole cache at each context switch, which is extremely expensive with modern big caches, or augmenting the tag with an address space identifier that is unique to each virtual address space. But these solutions usually have a high cost, and \gls{VIVT} caches are rare.\\

\gls{PIVT} caches suffer from the same problems of \gls{VIVT} caches and in addition require the \gls{TLB} translation to access the set, and are not used in practice.

Finally, \gls{VIPT} caches can access the set in parallel with the \gls{TLB} translation and do not suffer from the homonyms problem, but can still have synonyms, which is an undesirable situation. Due to their small latency, \gls{VIPT} caches are currently used for the lowest level of the cache hierarchy, where access speed is fundamental (see \autoref{sec:cache_hier}).

\subsection{Other types of cache} \label{cache_others}
Other types of cache exist. A first type is the \emph{victim cache}, which holds the lines evicted from the main cache; since this cache is accessed only after looking up the data in other caches, it can have a higher penalty and thus a higher associativity. This cache serves as an added layer between the normal cache and the main memory, can have a huge size (from 32 to 128 MB) and can be implemented with a technology having a smaller cost in terms of area and energy, like \gls{eDRAM}.

Caches can be specialized to store only some type of data, for example those needed in particular operations. \emph{Trace caches}, for example, store small traces of instructions that are executed often. Similarly, a \emph{micro-operation cache} stores instructions that have already been partially decoded: these caches are typical of the modern x86 architectures, where the variable-length instructions are pre-decoded into fixed-length micro-instructions before entering the \gls{CPU} pipeline. To speedup the slow decoding phase of variable-length instructions, groups of corresponding micro-instructions are stored inside such a cache and looked up before proceeding with a new translation, thus reducing the bottleneck of decoding.

\section{Cache hierarchy} \label{sec:cache_hier}
Designing a cache is always a trade-off between latency, area and performance, indicated in terms of number of hits over the number of accesses (\emph{hit ratio}). With the increment of the \gls{CPU} frequency that occurred in the early 2000s and later with the advent of \glspl{CMP}, the role of the cache has become increasingly important in leveraging the power provided by the computational resources. Therefore the cache had to scale-up with the rest of the architecture, without causing bottleneck effects when the cores load data from memory. Since the latency of a single cache is determined by the number of sets and the associativity (see \autoref{subsec:cache_design}), even with the last technological enhancements it is not possible to build a unique cache with high size, high associativity and small latency. Thus, designers decided to organize different caches into a \emph{hierarchy} of layers, where each layer has size and latency greater than the previous one.

\gls{L1} caches are the first ones to be accessed for lookup and are very small, typically 32 or 64 KB, and usually divided into a \emph{data cache} and an \emph{instruction cache} (realizing a so-called \emph{Harvard architecture}). The first one contains only the applications data, which often exhibit higher locality than instructions; to leverage this fact, data caches can have higher associativity than instruction caches (for example, 8 against 4). In order to limit latency (around 2 cycles), the \gls{L1} cache has small size and employs a \gls{VIPT} addressing scheme, so that no homonyms issue occurs and the set access is performed immediately. In \gls{CMP} architectures, \gls{L1} caches are per-core, in order to provide to the core datapath a dedicated memory and avoid contention with other cores.

In case of cache miss in the \gls{L1} layer, the subsequent \gls{L2} cache is accessed, which usually is bigger (from 256 KB to 8 MB) and has higher associativity (around 8), but also higher latency (from 5 to 12 cycles). This layer of the hierarchy employs the \gls{PIPT} scheme as the \gls{TLB} translation is  performed in parallel with the \gls{L1} access, so that no overhead is required to handle aliasing. If this cache is the last layer in the hierarchy, it is shared among more cores (typically all, but sometimes only between two, so that two separate slices of \gls{L2} cache may exist), otherwise it is per-core.

Modern \glspl{CMP} have an upper \gls{L3} cache with even greater associativity (from 12 to 16), size (from 8 to 32 MB) and latency (from 10 to 20 cycles) and present thus a high hit ratio. This level is usually the \gls{LLC}, and is connected to the main memory via a controller (with 100 cycles or more of latency); it is usually shared by all the cores, but may rarely be divided into different, independent slices shared between couples of cores.

Because of the increasing number of cores and of the increasing \gls{LLC} size, this last layer is split into more slices (or tiles) that are interconnected with the cores. Therefore, the cores may experience contention on the intercommunication while loading data from the \gls{LLC}.

Finally, \glspl{CPU} can have other specialized caches to enhance some stages of the datapath (like a micro-instruction cache) or an upper victim cache.

\section{Contention on a Shared Cache} \label{sec:contention}
To understand the nature of this work and of those presented in the next chapter, we quickly review how contention can arise on shared caches. Given the considerations on modern \glspl{CMP} presented in the previous section, we assume that only the \gls{LLC} is shared and we focus on this level; but the same considerations apply as well to other shared layers. Since the \gls{LLC} is shared by multiple cores (usually all), the different applications executing simultaneously may be slowed down when conflicting for this resource. Lower layers indeed are more isolated as they are per-core, and do not cause bottlenecks.\\
Measuring and modelling contention on shared caches is the objective of previous work \cite{Fedorova:2010:MCS:1646353.1646371}, which models the sensitivity of an application to cache sharing and its impact on other applications.

Focusing on the \gls{LLC}, contention is a phenomenon well known in the literature. It is due to several factors, both hardware and software.

On the hardware side, the main factor is the actual design of caches, where the mapping of the data from main memory depends on the memory address. Assuming a physically mapped cache, some of the bits of the memory address determine the cache set the referenced data are mapped to; if two applications are mapped to memory addresses with these bits being equal, their data will be mapped to the same cache set and contention may occur.\\
Commodity \gls{CMP} architectures lack interfaces to control the cache mapping. Instead, other architectures provide simple interfaces for this purpose \cite{octeon}, but on \glspl{CMP} currently found in servers and workstations such interfaces are not provided nor planned for the future (an exception is the \gls{CPU} used by Cook et al. \cite{Cook:2013:HEC:2485922.2485949}, but this modified processor is not available on the market).\\
Another hardware source of \gls{LLC} contention is the \gls{LRU} policy employed to choose the cache lines to be evicted. The \gls{LRU} policy is based on the past behavior of the application: thus, with a single application, it is effective in leveraging temporal locality. With the advent of \gls{CMP} architectures, a shared cache is accessed by multiple applications: hence, a set may respond to requests coming from several applications, thus mixing lines referenced with different access patterns. To optimize the data placement, it would be necessary to predict the locality of the lines. However, the \gls{LRU} policy is unable to predict the future usage of lines already in cache and thus may replace them with others having lower locality; in this case, the policy goes against the final objective of the cache itself. Non-optimal data fetching is exacerbated by cache prefetchers, whose aggressive behavior can load useless data evicting other applications lines. Furthermore, some applications have a working set bigger than the \gls{LLC}: even with very high locality, two applications may conflict because of the limited \gls{LLC} size.

On the software side, the basic factor that exacerbates \gls{LLC} conflicts is the poor locality of co-located applications, which causes the \gls{LRU} policy to be ineffective. In addition, different execution phases of the same application may change the temporal locality of the data accesses and determine cache conflicts that change in time. Another factor is the OS layer, which schedules applications without taking into account contention.

Apart from its cause, cache contention belongs to two categories: \emph{thrashing} and \emph{pollution}.\\
Thrashing \cite{Denning:1968:TCP:1476589.1476705} refers to lines with high locality being evicted by other lines with high locality, because both sets of lines are mapped to the same cache set, which experiences a high memory pressure that causes the continuous eviction and reloading of data. These frequent transfers to and from memory stress the memory controller, which becomes the performance bottleneck. Thrashing is typical of co-scheduled (i.e., running at the same time on cores of the same \gls{CMP}) memory-intensive applications with good locality and it is hardly predictable as it depends on the physical location of data in main memory, in turn depending on the OS, the workload, etc.\\
Pollution refers to the eviction of lines with low locality due to the insertion of other lines with low locality: since the \gls{LRU} policy is unable to predict the future usage of inserted lines, it must evict more useful ones to make room for the formers. The evicted line, assuming they have higher locality, will be reloaded to the cache, causing contention on cache sets and on the memory controller.



\section{Buddy memory allocator} \label{sec:buddy}
Since modern shared caches are \gls{PIPT}, the cache set where data are placed depends on their physical address. In turn, physical addresses depend on a component of the \gls{OS} called \emph{physical memory allocator}, which manages the physical memory of the machine. When physical memory is needed, various subsystems of the kernel (the page fault handler, \gls{DMA} drivers, etc.) issue a request to the physical memory allocator, which allocates a contiguous physical area of at least the requested size.

Physical areas must be managed at a minimum granularity that is determined by the \gls{CPU} architecture. Physical areas of this minimal size are called \emph{physical pages}, while the areas with the same size in virtual memory are called \emph{virtual pages}. Both physical and virtual pages are memory-aligned, so that the address of a byte inside a page can be split into two bit fields: the most significant bits refer to \emph{page address}, which identifies the page, and the less significant bits refer to the \emph{page offset}, which identifies the referenced byte.\\
Mapping the addresses of virtual pages to those of physical pages is the task of the \gls{TLB}. Because of the memory-alignment of pages, the page address to be translated is found by re-routing the most important bits; once this virtual address is translated into a physical address, it is re-padded with the offset bits. Commodity \glspl{CPU} have a page size of 4 KB, thus with 12 bits of page offset and 52 bits of page address (in 64 bit architectures). Modern \glspl{CPU} also support other page sizes, as discussed in \autoref{sec:hugepages}.

A physical memory allocator is designed with several goals in mind:
\begin{itemize}

\item the first goal is \emph{efficiency}: when a page is requested, it must be returned as fast as possible

\item \emph{scalability} is also important, as machines contain very different amounts of memory (from few MB to hundreds of GB) that the allocator must manage efficiently

\item another goal is keeping \emph{internal fragmentation} low: internal fragmentation is the waste of memory because of the granularity of allocation (depending on the page size)

\item also \emph{external fragmentation} must be limited: external fragmentation refers to the unavailability of large contiguous memory areas due to the interleaving between free and used areas

\end{itemize}

Internal fragmentation is high when the allocated areas have size very different from the requested amount. Allocators try to keep this phenomenon low by reserving a space that is the closest possible to the one requested. However, the granularity of the page imposed by the hardware inherently causes some internal fragmentation, which, for requests smaller than the page size, is handled by the upper layers (like the applications libraries).

External fragmentation plays a more important role in modern computers, where a large amount of memory is often available. This fragmentation prevents the allocation of large memory areas to sub-systems that need it (like \gls{DMA} drivers). External fragmentation may cause performance degradation of the allocator because it must manage many small memory fragments in its own data structures.

In Linux, the heart of the physical memory allocator is the Buddy algorithm \cite{buddy_allocator}. It is known in the literature from almost 40 years and is widely used because of its features. This algorithm is very efficient, and is effective in limiting external fragmentation. Moreover, it has shown to scale well on a wide range of machines equipped with very different memory amounts \cite{alg:245}.\\
The following sections show the functioning of this algorithm with reference to the Linux kernel: in particular, \autoref{subsec:buddyalg} explains the data structure the algorithm is based on, while \autoref{subsec:buddyalg} explains how this data structure is leveraged to perform the typical operations of the Buddy algorithm.

\subsection{Buddy data structure} \label{subsec:buddydata}
The main data structure of the Buddy algorithm is an array of doubly-linked lists, as depicted in \autoref{fig:buddystruct}; each list links groups of physically contiguous pages. A single group is called \emph{buddy}, and has a certain size that is defined by a parameter called \emph{order}: a buddy of order $ i $ is composed of $ 2^i $ physically contiguous pages.
To represent a buddy of order $ i $, the simplest way is using the page address of its first page as identifier, called \emph{buddy address}; like memory pages, also the buddies are memory-aligned, so that the less significant $ i $ bits of the buddy address are set to 0. The Linux kernel uses this representation, which makes it easy to get the buddy address from the address of any of its pages, given the order.

The order of the buddy determines the list it is stored in, so that the proper list can be accessed in constant time via an addition to the array base pointer. Inside each list, the buddies are doubly linked in order to iterate easily among them. When a page is released, it is added to the head of the list, in order for a page insertion to be performed in constant time. Similarly, a page removal can be done in constant time by picking the head of the list.

\begin{figure}
\centering
\includegraphics[width=.8\textwidth]{img/buddy_data.eps}
\caption{Lists of free areas of the Buddy allocator}
Different groups of contiguous physical pages (\emph{buddies}) are stored in different lists on the basis of their \emph{order}.
\label{fig:buddystruct}
\end{figure}

\subsection{Buddy algorithm} \label{subsec:buddyalg}
The Buddy allocator responds to memory requests by allocating buddies of a certain order. To fulfill a memory request, the Buddy allocator allocates the smallest buddy whose size is greater than or equal to the requested amount. Allocating the least amount of physical space (compatible with the buddies sizes) is a way to limit internal fragmentation, though this objective is partially left to higher levels (e.g. kernel/userspace allocation calls like \code{malloc()}).

Since the size of a buddy is a power of 2 and is determined by its order $ n $, the Buddy algorithm uses $ n $ to access the list of all buddies of order $ n $, and picks the buddy on top of the list. If no buddy is present, the allocator splits a buddy at order $ n+1 $ in two buddies of order $ n $, stores one of the two halves in the free buddies list of order $ n $, and finally returns the other. To split a higher order buddy, the allocator first checks the list of order $ n+1 $ not to be empty. If no buddy is present at order $ n+1 $, the list of order $ n+2 $ is accessed, and so on until a buddy is found. The algorithm splits this buddy in two halves, adds one of them to the free list of the lower order and further splits the other half; this splitting process is iterated until a buddy of size $ n $ is obtained.

Each buddy of order $ n $ is uniquely identified by the physical address of its first page, whose $ n $ less significant bits are 0 because of the memory-alignment of buddies. Hence, to find the two halves of a buddy it sufficient to consider only the $n$-th less significant bit of its address: if this bit remains 0, the address identifies the first half, and if it is set to 1 the obtained address identifies the upper half. Therefore, splitting a buddy can be done in constant time.\\

\begin{figure}
\centering
\includegraphics[width=.8\textwidth]{img/buddy_ops.eps}
\caption{Coalescing of two buddies}
After a buddy of order $ n $ is freed, it is coalesced with the ``twin'' buddy and the resulting $ n+1 $ buddy is placed in the proper list; the smaller buddies differ only in the least significant bit of their address; the coalesced buddy does not consider this bit.
\label{fig:buddyops}
\end{figure}

In order to limit external fragmentation and maintain scalability, in case of buddy insertion the buddy algorithm attempts to re-group buddies into bigger buddies. When a buddy of order $ n $ and address $ a_n $ is freed, the algorithm checks whether the ``twin buddy'' is free too, i.e. whether the buddy with the $n$-th less significant bit inverted ($ a_n \bigoplus 2^n $) is free. If this last buddy is not free, the freed buddy is inserted into the list of order $ n $ and the algorithm terminates. In the other case, depicted in \autoref{fig:buddyops}, the two twin buddies are removed from the free list of order $ n $ and coalesced into a single one of order $ n+1 $ and address $ a_{n+1} = a_n \land (a_n \bigoplus 2^n)  $; this bigger buddy must be inserted in the list of order $ n+1 $. Before the insertion, the algorithm again checks whether the twin buddy of order $ n+1 $ is free too, and the same procedure is applied.

In Linux, to look for the state of a buddy (free or allocated) there is no need to scan a list as the data structures to manage physical pages are indeed in an array in kernel space. All the physical pages are indeed described by the elements of a long array; these elements have a special data structure that describes the main features of a page (virtual address it is mapped to, swap information, usage, etc.), including whether it is the first page of a buddy: in case it is, also the order of the buddy is stored. Therefore, all the operations of accessing a buddy take constant time: in fact, to access the first page of a buddy of order $ n $ and address $ a_n $, it is sufficient to add $ S \times a_n $ to the base pointer of the pages array, where $ S $ is the size of the data structure describing a physical page. Hence, the operations of splitting and coalescing described above take constant time.

To limit external fragmentation by leveraging the coalescing of buddies, Linux applies a small optimization: after a buddy split, the allocator always returns the lower half, so that its counterpart is stored in the free list and available for a subsequent allocation, without the need of splitting another buddy. Always using  the same half has been shown to be more efficient and to decrease fragmentation, particularly with kernel drivers of fast devices that typically need large contiguous buffers \citep{buddyfirst}.

\section{Hugepages} \label{sec:hugepages}
\autoref{sec:buddy} shows how the kernel allocator manages physical areas of different sizes; yet, the mapping between physical areas and virtual areas is then managed by the \gls{TLB} at the granularity of the page, whose size is usually 4 KB. Therefore, a large memory area, even if physically and virtually contiguous, results in many mappings stored into the \gls{TLB}. This increases the probability of \gls{TLB} miss, penalizing applications with a big memory footprint.\\
To overcome this limitation, modern \glspl{TLB} can manage memory at a higher granularity. This feature can conflict with the technique we employ to partition the \gls{LLC}, as \autoref{subsec:consequences_hugep} shows. In this section, we give a quick overview of this feature of modern \glspl{CPU} in order to present the context of \autoref{subsec:consequences_hugep}.

As said, modern \glspl{CPU} allow bigger page sizes: for example, the x86 architecture allows page sizes of 2 MB and of 4 MB, \gls{ARM} allows pages of 2 MB and 16 MB and the IBM Power architecture allows pages of 64 KB, 16 MB and even 16 GB. Pages of those sizes are called \emph{hugepages} in the Linux terminology and \emph{large pages} in Windows terminology; in this work, we will refer to them as hugepages.

To exploit hugepages, the kernel must allocate a contiguous physical area of the same size, which is mapped into a corresponding virtual area. Also these pages must be memory-aligned, for the same reasons explained for 4KB pages.\\
Because of the size of hugepages, their offset field is longer than the that of 4 KB pages: while 4 KB pages have 12 bits of offset field, 2 MB pages have 21 bits of offset, 4 MB pages have 22 offset bits and 16 GB pages have 34 offset bits. This longer size of the offset field will introduce some problems, as anticipated above.

Hugepages support is actually limited both in Linux and Windows \glspl{OS}, as it requires deep modifications to the subsystems that manage physical memory, physical-to-virtual mapping and the \gls{TLB}. In Linux, for example, the use of hugepages for memory allocation must be explicitly configured by the system administrator. Nonetheless, the research community is investigating the benefits and drawbacks of this possibility, and some results have already been presented \cite{lu2006using,5197302}.
