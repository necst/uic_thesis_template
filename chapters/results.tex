\chapter{Experimental Results} \label{chap:results}
In this chapter we present the results obtained by experimenting with \system. \autoref{sec:testbed} explains the experimental environment used for testing \system, with a brief description of the hardware and software components and of the experimental methodology used to perform the tests. \autoref{sec:app_char} explains how applications are characterized and chosen in order to obtain the final evaluation of \system, which shown in \autoref{sec:app_iso}.

\section{Experimental environment} \label{sec:testbed}
This section explains the setup of the testing environment. To show the effectiveness of \system, we needed to run \gls{CPU}-intensive applications with the least possible disturbance, thus carefully setting the hardware and software of the platform.

\subsection{Testbed and coloring parameters}
The machine used as testbed is equipped with a single socket holding a quad-core \mycpu \gls{CPU} with a clock frequency of 2.93 GHz and 12 GB of \gls{RAM}. These parameters fairly represent the ones of current server systems, for which \system is meant. The \gls{CPU} has three layers of cache, with a fixed line size of 64 B:

\begin{itemize}
\item a shared \gls{L3} (\gls{LLC}) cache of 8 MB, with 8192 sets and 16-way associativity
\item a per-core \gls{L2} cache of 256 KB, with 512 sets and 8-way associativity
\item a per-core \gls{L1} instruction cache of 32 KB, with 128 sets and 4-way associativity
\item a per-core \gls{L1} data cache of 32 KB, with 64 sets and 8-way associativity
\end{itemize}

To measure the real effects of \system, we disabled the cache prefetchers, the Hyper-Threading support \cite{ht} and the Turbo Boost support \cite{turbo}. These features could indeed influence the applications performance and are not controllable in software nor measurable. In particular, cache prefetchers aggressively load data according to speculative policies, thus affecting the performance in an unpredictable way.\\
The physical page size of the architecture is the default one for x86 architectures, i.e. 4 KB. The parameters of interest for \system are the following:

\begin{itemize}
\item 12 bits of physical page offset
\item 6 bits of cache line offset
\item 13 bits of \gls{LLC} set number
\item 9 bits of \gls{L2} set number
\item 7 bits of \gls{L1} set number (considering only the instruction cache, the one with more sets)
\end{itemize}

from which we compute the fundamental parameters for page coloring:

\begin{itemize}
\item $ 13 + 6 - 12 = 7 $ bits of \gls{LLC} color
\item $ 9 + 6 - 12 = 3 $ bits of \gls{L2} color, i.e. the bits of \gls{L2} set number overlapping with the \gls{LLC} color bits
\item $ 7 - 3 = 4 $ bits of color, hence with the possibility of partitioning the \gls{LLC} cache in $ 2^{4} = 16 $ slices of 512 KB each one while avoiding \gls{L2} cache partitioning (the \gls{L1} set number bits don't overlap with these final bits); the color bits are those from the 15th to the 18th
\end{itemize}

On the software side, the Linux distribution installed is Ubuntu version 12.04 \gls{LTS}, and the kernel used is our custom \system version with the default parameters of Ubuntu distribution.
In particular, the number of buddy orders (\autoref{sec:buddy}) is the default one, i.e. 10. This implies that at most the 10 lower bits of the physical page address are 0 to indicate the buddy's first page (and thus the buddy itself). These bits, ranging from the 13th to the 22nd, completely overlap with the color bits, so that:
\begin{itemize}
\item the orders from 0 to 2 (included) have complete lists of 16 colors
\item the orders from 3 to 5 (included) have half of the colors of the previous order (from 8 to 2)
\item the orders from 6 on have only one color
\end{itemize}

\subsection{Test applications}
It is fundamental for showing \system features to stress the \gls{CPU} and memory subsystems with computational-intensive applications, possibly having different characteristics of cache-friendliness to shape a well-mixed workload and stress the different parts (\gls{LLC}, memory controller, etc.). Moreover, it would be useful to use a well-known set of applications for a comparison with other works. For these reasons we choose the \gls{SPEC} \gls{CPU}2006 benchmark suite \cite{spec}, which is a reference suite for many research works \cite{Cook:2013:HEC:2485922.2485949}. This suite has a large set of applications from multiple fields having different characteristics with respect to the usage of the \gls{CPU} resources.\\
Since many applications use more input sets sequentially to perform the tests, they can present different phases that would make the measurements collections of different, heterogeneous and not equally distributed data. Therefore, we chose to split all the possible inputs of the applications and to evaluate each one separately. In particular, we selected 9 benchmarks with different usage patterns in order to have a reasonable but representative set of applications with respect to their cache usage. For each application we chose the input set causing the longest run. The selected applications and inputs are reported in \autoref{tab:spec_apps}

\input{chapters/spec_apps}

\subsection{Experimental methodology and tools}
Once the applications are selected, we illustrate the steps performed for testing the effectiveness of \system and how they are performed.

To launch an application and give it a \gls{LLC} partition, a small launcher application has been developed called \code{rlaunch}, which takes the number of colors to be used and the application to run. \code{rlaunch} executes the \system system call \code{sys_use_cache_colors()} passing the color number and then calls the \code{exec()} system call to run the target applications, which ``inherits'' the cache partition from the caller.

The application profile is built by recording data via the \code{perf stat} tool provided by Linux \cite{perf}, which exploits the hardware performance counters available in the x86 architecture to measure the per-thread performance. For our experiments, we measured the IPC, the number of \gls{LLC} accesses and the number of \gls{LLC} misses. It is important to remark that, as our \gls{CPU} has four hardware performance counters, the count of events has not been sub-sampled and the reported measures are real measures, not obtained by multiplexing.

All the run applications have been forced to run on a fixed core through the \code{taskset} command to avoid any overhead due to moving the application and reloading the data into the \gls{L2} and \gls{L1} caches. Finally, any power saving mechanism has been disabled using the  \code{cpufreq-set} command and setting the \code{performance} \gls{CPU} governor, which sets the maximum allowed frequency for all the cores.

\section{Application characterization} \label{sec:app_char}
The first step is to build an accurate profile of the applications behavior with respect to varying size of caches. These profiles will indicate the sensitivity of each application to the assigned cache space and will guide the choice of suitable workloads to show the effectiveness of the performance isolation \system provides.

\begin{figure}
\centering
\includegraphics[height=.9\textheight]{img/app_char.pdf}
\caption{Applications profiles with different cache partitions.}
\label{fig:app_char}
\end{figure}

Therefore, we built the cache usage profile of the target applications by assigning them \gls{LLC} partitions of different cache size. Each application was run separately, without any significant application running at the same time. For the various set points, 10 measurements were collected, which are summarized in \autoref{fig:app_char}. This figure shows the cache profiles with cache partitions sizing from 0.5 MB (1 color) to the whole cache (8 MB - 16 colors) with step of 0.5 MB.
A particular case is 429.mcf, which needs at least 1.8 GB of \gls{RAM} memory on 64 bits architectures to run. Because of the limitations found in \autoref{subsec:consequences_mem}, 429.mcf must receive at least 1.5 MB of cache, corresponding to 2.25 GB of memory.\\
\autoref{fig:app_char} plots the averaged data points with the interval bars, with time slowdown percentage (red line), \gls{LLC} miss rate (blue line) and \gls{L2} miss rate (green line). What immediately strikes are the plots of 450.soplex and 482.sphinx, whose interval bars indicate a huge variability of the collected data. As these phenomenon is not seen in the other profiles, it is likely to be caused by a random behavior of the two applications, whose duration is about one order of magnitude bigger than the others (this fact could worsen the effects of a random pattern in data access). In particular, 450.soplex works with a high-dimensional sparse matrix and 482.sphinx emplys randomization to sample the input set repeatedly \cite{Henning:2006:SCB:1186736.1186737}. However, we are actually investigating these behaviors, in order to gain a clearer insight on the \gls{LLC} usage.\\
At a general glance, the collected profiles confirm our good choice of a variegated set of tests, where some like 401.bzip and 471.omnetpp have great benefits from more cache space, while others have almost none like 462.libquantum and 437.leslie3d.

Furthermore, we measured the \gls{L2} miss rate, which is represented in \autoref{fig:app_char} with a green line. The fact that this measure remains almost constant in all the measurements of each application ensures the \gls{L2} cache is used completely, thus avoiding performance bottlenecks due to a bad partitioning.


\section{Isolation of co-running applications} \label{sec:app_iso}
With the cache profiles collected previously, we can choose a number of workloads with mixed applications characteristics. The aim of this choice is to run the applications simultaneously on different cores, isolating one of them in \gls{LLC} and measuring it. Therefore, to exploit the potential benefits provided by \system the target application must be sensitive to cache space, while the others should be varied in order to see how \system effectively partitions the \gls{LLC}.
To choose the workloads, we classified the applications into three categories according to their ``sensitivity'' to the \gls{LLC} partition size. This feature can be indicated by the reduction of slowdown with various \gls{LLC} partitions, as previous work does \cite{Cook:2013:HEC:2485922.2485949}. In more details, we looked at the difference in slowdown between the run with 0.5MB of \gls{LLC} and that with the whole cache. According to this value being in certain ranges, we categorized the tests as \emph{red}, \emph{yellow} or \emph{green}, with decreasing cache sensitivity. The classification is shown in \autoref{tab:spec_cat}, with the thresholds that define the category.

\input{chapters/spec_categ}

With respect to the values chosen for the ranges, 450.soplex and 482.sphinx appear as corner cases, but have been classified as Yellow because of their high variability, which sometimes prevents them from exploiting the \gls{LLC} as ``true'' Red applications (if we consider the worst case, the slowdown difference is in fact below the threshold for the Red category).\\
From these categories, 10 workloads have been chosen to be tested. Red applications, being the most sensitive ones, are granted an \gls{LLC} partition and their performance is measured. The primary criterion to choose the applications' mix for each workload is mixing cache-sensitive applications, so that the competition among the applications, which would heavily penalize the target application, can result effectively prevented by \system. After all the Red applications have been chosen, then the Yellow ones are chosen as ``polluters'' and finally the Green ones are used.\\

\input{chapters/spec_wl}

The experimental methodology consists in running all the applications of each workload simultaneously, each application being kept on a specific core and the target application being colored. In case a polluter terminates before the target application, it is immediately restarted. The measurements with the whole cache assigned to the target applications have been skipped, because in such situation \system would allocate pages for all the applications from all the colors, thus denying isolation.
The whole experiment is repeated 10 times, and the results are shown in \autoref{fig:wl_char}, where the dashed lines report the profile of the target application when running alone (those in \autoref{fig:app_char}).
As visible from experiments, the curves of the co-located applications roughly follow those of the solo execution and in some cases there is a good overlapping between the \gls{LLC} miss rate curves. This fact indicates that \system  is effectively able to reserve an \gls{LLC} partition and thus make the application benefit from isolation. The distance between the slowdown curves is instead due to the non-partitionable resources, in particular the on-chip bandwidth and the memory controller.



\begin{figure}
\centering
\includegraphics[height=.9\textheight]{img/wl_char.pdf}
\caption{Workloads profiles with different cache partitions.}
For each workload, the target application is shown. The continuous line indicates the workload, the dashed line the target application profile from \autoref{fig:app_char}
\label{fig:wl_char}
\end{figure}