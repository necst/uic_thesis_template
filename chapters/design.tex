\chapter{Design} \label{chap:design}
This chapter shows the design of our proposal, which is a modification of the Linux kernel to implement page coloring. Our solution is aimed at guaranteeing to applications isolation in the \gls{LLC}. Its design is based on the architecture of modern \glspl{CMP}, with a careful study of which constraints these architectures pose to our proposal. Furthermore, we also study the limitations our modification poses on the applications, in order to show the main trade-offs needed to employ page coloring on modern architectures.\\
In particular, \autoref{sec:vision} will explain the overall vision behind our work, while \autoref{sec:approach} reports more details about the design with respect to the Linux kernel. \autoref{sec:cache_model} discusses the assumptions on the \gls{LLC} at the base of our proposal and the limitations they impose on our work, while \autoref{sec:consequences} shows how page coloring impacts on applications. Finally, the modifications to the Buddy data structure and to the allocation algorithm are explained in \autoref{sec:buddy_mod}.

\section{Vision} \label{sec:vision}
The overall vision behind this thesis is to let cloud platforms provide better \gls{QoS} via isolation of running applications.
A way to ensure \gls{QoS} is partitioning the hardware resources shared by co-running applications. Some techniques like time sharing mechanisms already exist from a long time, but cannot avoid the effects of contention exposed in \autoref{sec:contention}. Moreover, partitioning is actually limited to a coarse granularity, because some resources are partitioned and others are not. Unpartitioned resources make performance unstable, increasing the variance with respect to an ``ideal`` partitioning of resources. Therefore, it is necessary to partition those hardware resources where most of the conflicts arise, taking into account the various layers of the architecture, to achieve a better \gls{QoS} with modern \gls{CMP}.

One of these resources is the cache, where contention occurs (as in \autoref{sec:contention}), thus affecting application performance. In particular, the \gls{LLC} is shared among the cores, leading to conflicts among applications that force reloading data from main memory; this in turn increases the latency of each process in an unpredictable way, as the conflicts depend on the mix of co-running applications. What we want to achieve is a complete isolation of applications in the \gls{LLC}, in order to make their performance predictable. This would allow us to assign to an application a \gls{LLC} partition of a suitable size, in order to achieve a certain \gls{QoS}. This goal must be achieved when the application is co-located with others on the same \gls{CPU}, which is the scenario we consider in this work.

Ideally, we would like to achieve predictable performance for mixes of applications sharing a single commodity \gls{CMP}, like those powering data centers, without any modification to either runtimes (e.g., the Java virtual machine) or applications. The implementation of page coloring within an OS makes a good fit, since it just requires updating the OS and profile applications to understand their resource requirements.
 
\section{Approach} \label{sec:approach}
Our goal is to partition the \gls{LLC} having low impact on existing platforms and taking into account the cache hierarchy of \glspl{CMP} architectures. Given this goal, a software technique is a mandatory choice; supporting page coloring is the only solution to \gls{LLC} partitioning in software.
Page coloring requires deep modifications to the physical memory allocator, a core part of any operating system that is not configurable nor ``pluggable'' at runtime.

Performing such a deep change requires the modification of the kernel parts that manage the physical allocation and the processes. Furthermore, our focus on cloud infrastructures leads us to use an hypervisor for this work. Thus, the available choices for our target kernel are Linux and Xen, while other solutions like VMware ESX and Microsoft Hyper-V are impractical since the source code is not freely available. We choose Linux, released under \gls{GPLv2} license, because it allows us to make prototypes without the need of a dedicated development environment.

Because of this choice, our design is affected by the architecture of the Linux kernel. The system we designed is called \system and accounts for the following objectives.
Introducing page coloring into the Linux kernel in order to \emph{isolate} the \gls{LLC} partitions of co-running applications requires changes to the Buddy data structures, storing free pages, and to the algorithm that performs allocation and de-allocation (\autoref{sec:buddy_mod}). These components are at the core of physical memory management, and any modification to them must be carefully studied.\\
In fact, physical memory management is a frequently accessed subsystem, involved in the page fault mechanism that allows Linux to grow and shrink the physical memory of a task (i.e., process, thread or \gls{VM}).

Despite the low level implementation of \system, our aim is to provide an easy interface to processes wishing to exploit its novel functionality.

Given these guidelines, the present chapter explores the main design decisions behind \system.
\autoref{sec:cache_model} explains the basic assumptions \system relies on, in particular those about the cache hierarchy, and the constraints imposed by the architecture; \autoref{sec:consequences} investigates the limitations of page coloring on \gls{CMP} architectures and finally \autoref{sec:buddy_mod} explains the modifications to the Buddy algorithm made to implement page coloring.

\section{Cache hierarchy model and consequences} \label{sec:cache_model}
This section explains the major assumptions at the base of \system and what are their consequences. In particular, \autoref{subsec:assumptions} goes through these assumptions in the context of a generic \gls{CMP} architecture with multiple layers of cache. Instead, \autoref{subsec:hier_part} shows the consequences of page coloring on the assumed caches hierarchy, with the possible drawbacks.


\subsection{Assumptions on the cache hierarchy} \label{subsec:assumptions}
Devising a model of our target cache hierarchy is an essential task to go through the design of \system. On one side, this model reflects the cache hierarchies available in commodity \glspl{CPU}, such as the layering of caches and their interconnection with the cores. On the other side, it is general enough to be applied to a wide class of commodity \gls{CMP} architectures.

First, we assume an architecture with multiple cores and multiple layers of cache.
The generic model of each layer of the cache hierarchy is a \emph{set associative} cache; each layer of cache has specific values of associativity and number of sets, but the line size is assumed to be constant across all the layers and to be equal to $ L $. We immediately note that modern architectures have a first cache layer designed according to a Harvard architecture, with two separate caches for code and data. This detail can be considered in our model, but will not impact on the design of \system.

Two assumptions are at the base of \system: the \gls{LLC} is physically indexed and unique, and is shared among the cores. These two assumptions are important to control the placement of data in the cache and thus to obtain an effective partitioning.
In particular, the first assumption cannot be relaxed: denying it means using a virtually indexed cache, which can be controlled only through the virtual memory address space of the application. Yet, this virtual space is under the control of the applications loader (or of the runtime environment, like the Java virtual machine) and only partially under the control of the OS; this would cause some of the memory regions of the process (like the text segment) not to be manageable and so not to be partitionable. Therefore, the physical indexing of the cache is a fundamental feature. The other key requirement is that the \gls{LLC} is shared among the cores through the lower levels of per-core caches. This assumption simplifies the management of the physical memory as the system deals with a unique cache, and makes partitioning the \gls{LLC} necessary to guarantee isolation.\\

These assumptions fit a wide variety of commodity architectures, like those if Intel, ARM and IBM. Some architectures like IBM Power6 and Intel Crystalwell (a variant of Haswell) also have an higher layer of cache used as a victim cache, which is outside our model. Because of the different nature of these memories, which act solely as a victim buffer for the lower levels of the hierarchy, modeling them is not needed for the design we propose. Hence, the lower level to which they are connected is assumed to be the \gls{LLC} and to be directly connected to the main memory controller.

It is important to stress that the physical indexing of the \gls{LLC} is a feature found in all modern \glspl{CMP} because of the small latency of the \gls{TLB}. For the same reason, also the lower level cache (usually a per-core \gls{L2} cache) is often physically indexed. The consequences of this fact are discussed in the following section.

\subsection{Partitioning the hierarchy} \label{subsec:hier_part}
Page coloring is the only software technique available to partition the \gls{LLC}; this restricts the address bits that can be used for partitioning, limited to those in common between the \gls{LLC} cache set bits and the page address bits (\autoref{fig:coloring}).
Yet, cache allocation is subject to another constraint. Assuming three layers of caches, partitioning the \gls{LLC} could impact also the use of the \gls{L2} cache. \autoref{fig:addr_cachel2} shows the use of the physical address to access the \gls{L2} cache of our target platform, which differs from \autoref{fig:addr_cache} in the number of bits of set number: here the set number consists of 9 bits.

\begin{figure}
\centering
\includegraphics[width=.8\textwidth]{img/addr_cachel2.eps}
\caption{Physical address bit fields for \gls{L2} cache access}
\label{fig:addr_cachel2}
\end{figure}

Among these bits, some may overlap with the \gls{LLC} color bits, as in \autoref{fig:color_overlap}; in this figure, the 2 most significant bits of the \gls{L2} set number overlap with the \gls{LLC} color bits. Let the \gls{LLC} color bits be $ c_{LLC} $ bits (in \autoref{fig:color_overlap}, they are the 7 color bits identified in \autoref{fig:coloring}) and the overlapping bits be $ v $.

\begin{figure}
\centering
\includegraphics[width=.8\textwidth]{img/color_overlap.eps}
\caption{Overlap of color bits and L2 set bits}
\label{fig:color_overlap}
\end{figure}

This overlap causes also the lower level of cache to be partitionable through the subset of $ v $ bits just identified. If we assume that only one thread runs on each core, partitioning this level of cache can penalize the running thread as it would restrict the data available in this layer; this, in turn, increases the accesses to the \gls{LLC} and hence the latency of data fetching operations. Instead, partitioning the \gls{L2} cache may be reasonable with \gls{SMT} architectures, but we discuss the partitioning of the \gls{L2} cache in \gls{SMT}-enabled cores in \autoref{sec:future}. In this work, we chose not to partition this layer and thus not to use the $ v $ bits. This decision is another key feature of \system, that ensures the full exploitation of the lower cache levels capacity in \gls{CMP} architectures. The remaining color bits are thus $ c = c_{LLC} - v $, and are those effectively usable for \gls{LLC} partitioning. The same check should be done for all the cache layers, but the lowest level (the \gls{L1} cache) is usually virtually indexed, so that partitioning the upper layers does not influence the data placement inside this layer. But even if this layer was physically indexed, the set number bits of both the \gls{L1} code and data caches are much less than those of the \gls{LLC} and do not overlap with the color bits.

\section{Consequences of page coloring on applications} \label{sec:consequences}
The tight relation between \gls{LLC} mapping and physical page address identified in \autoref{subsec:hier_part} poses some limitations to the applications to be isolated. These limitations are discussed in this section.\\

\subsection{Cache-Memory constraint} \label{subsec:consequences_mem}
The first limitation is that the physical memory available to the application is constrained by the percentage of cache allocated to it. If we assume the amount $ N $ of physical memory pages to be an exact multiple of the number of colors $ N_{c} = 2^{c} $, then each color corresponds to $ \frac{N}{N_{c}} $ pages and a corresponding amount of memory of $ \frac{N}{N_{c}} \times P $ bytes ($ P $ beig the page size). Hence, if an application is reserved a percentage $ k $ of \gls{LLC} and thus $ n = k \times N_{c} $ page colors, it can allocate at most $ n \times \frac{N}{N_{c}} $ pages corresponding to $ n \times \frac{N}{N_{c}} \times P $ bytes of main memory. Relaxing this constraint by using pages of the same color for different applications is not considered, as it would force applications to share \gls{LLC} sets and would prevent perfect isolation.

This memory constraint can theoretically be a limitation for applications with a big memory working set and a small cache working set; to fit the cache usage of these applications, it would be natural to devote them a small partition and so a small number of colors, thus restricting the physical memory available to the application. In such case the Linux kernel would swap to the disk many memory pages of the application, causing an intolerable performance penalty. Furthermore, if the available memory is much less than the requested memory the kernel could even fail in allocating pages, leading to crashes. Yet, applications with a big memory footprint and small \gls{LLC} usage are rare in practice, as the \gls{LLC} footprint is roughly proportional to the memory occupation.

A second drawback is that if the amount of memory is not an exact multiple of $ N_c $, some colors are associated to a smaller amount of pages while others to a greater amount; the \gls{LLC} sets corresponding to these last colors would thus experience a higher pressure in terms of accesses and of evictions. This situation is however unlikely to happen because of the huge amount of main memory installed in current machines, which gives applications a lot of memory and so a huge number of memory pages to ``spread'' their data on, thus avoiding pressure on small areas. For example, with 12 GB of main memory installed and $ k = \frac{1}{8} $, 1.5 GB is available to the application.

\subsection{Limits on physical page size} \label{subsec:consequences_hugep}
The second limitation is the impossibility of using hugepages (see \autoref{sec:hugepages}), available in some modern architectures.\\

\begin{figure}
\centering
\includegraphics[width=.8\textwidth]{img/hugepages.eps}
\caption{Bit fields for hugepages and \gls{LLC}.}
The color bits are highlighted and do not overlap with the page address, thus making page coloring unfeasible.
\label{fig:hugepages}
\end{figure}

As from \autoref{fig:hugepages}, the very different granularities of hugepages and of the \gls{LLC} management cause the color bits to overlap entirely with the page offset; therefore, a single hugepage covers all the possible colors of the platform and is mapped to all the cache sets, thus denying isolation of different applications.\\
This is the main limitation of \system and cannot be relaxed because of the strict correspondence between physical memory and \gls{LLC} mapping. Given the size of modern \glspl{LLC}, the only way to overcome this limitation would be adding hardware interfaces to control the \gls{LLC} mapping. Otherwise, architectural modifications like a bigger line offset, more \gls{LLC} sets or more granular page dimensions could make hugepages available with \system. But all these features would have a big impact both on \gls{CMP} and on \gls{OS} architectures and have not been planned by \gls{CPU} designers.


\section{Rainbow Buddy} \label{sec:buddy_mod}
Implementing page coloring requires a modification of the Buddy algorithm and data structure, which have been explained in \autoref{sec:buddy}. The aim of these modifications is to permit the allocation of a page of a specified color. The modifications are based on the \gls{LLC} parameters identified in \autoref{sec:cache_model} and have a high impact on the Buddy allocator: in more details, \autoref{subsec:buddydata_mod} explains how the data structures are modified while \autoref{subsec:buddyalg_mod} explains how the basic algorithms of insertion and removal are modified.


\subsection{Data structure modifications} \label{subsec:buddydata_mod}

\begin{figure}
\centering
\includegraphics[width=.55\textwidth]{img/buddy_colored.eps}
\caption{Rainbow Buddy data structure.}
The designed data structure with colored lists per-order; in the higher levels multiple colors are aggregated.
\label{fig:buddycolored}
\end{figure}

The data structure of the Buddy system must be modified to allow insertion and removal of pages of a specific color. The color is specified in the request: thus, it is natural to divide each order list into several sub-lists, one per color. Therefore, the \system Buddy allocator uses an array of lists, which can be accessed in constant time by adding the color number to the base pointer.\\
More colors can be specified in the request, for example when the application to be isolated is given more colors and any one of them can be used. In this case, a drawback of the new data structure is the need of iterating through multiple lists to search for an available buddy. To avoid this, we also decide to store the number of buddies for each order, so that in case no buddy is present the algorithm immediately looks for a buddy of higher order to be split.

\begin{figure}
\centering
\includegraphics[width=.8\textwidth]{img/buddy_color.eps}
\caption{Overlap of color bits and buddy bits}
The complete overlap determines the division into arrays of different size of the Buddy order lists.
\label{fig:buddycol}
\end{figure}

\autoref{fig:buddycol} shows a possible configuration of the bit fields used to identify the buddy, with the page color highlighted; the color bits depend on the parameters identified in \autoref{sec:cache_model}. Because of the limitations discussed in \autoref{subsec:hier_part} on the color bits, these bits may completely overlap with the ``buddy order'' bits. These bits (also highlighted in \autoref{fig:buddycol}) are the ones that must be 0 in the buddy address, and their number depends on the buddy order, as \autoref{sec:buddy} explains; \autoref{fig:buddycol} highlights all the possible buddy bits, assuming that the maximum allowed order is 10.

The overlap of buddy bits and color bits affects the structure of \system Buddy. If we assume that no bits overlap, which happens for buddies of low orders (in \autoref{subsec:hier_part} from order 0 to order 2, included), than the buddy address contains a specific color; in this case the buddy spans only that single color. Since all the $ N_{c} $ possible colors are available under this hypothesis, the lists at these orders must be split into $ N_{c} $ sub-lists, one per-color.\\
Instead, if we assume that only 1 bit overlaps with buddies of a certain order $i$ (order 3 in \autoref{fig:buddycol}), the possible colors allowed are $ \frac{N_{c}}{2} $, and thus the per-color lists at $i$-th order must be only $ \frac{N_{c}}{2} $. Similarly, if $n$ colors overlap for a certain order, its sub-lists must be $ \frac{N_{c}}{2^n} $. Finally, if all the bits overlap, the corresponding orders have only one sub-list (this happens in \autoref{fig:buddycol} for orders from 7, included, to 10). The different lists are exemplified in \autoref{fig:buddycolored}.

\subsection{Algorithm modification} \label{subsec:buddyalg_mod}
To insert a buddy, its color is found from its address and is used to access the proper color list of the buddy order; then, as before, the buddy is inserted on the head of the list. To get the color of a buddy it is sufficient to extract the color of its first page (which identifies the buddy itself) and right-shift this color of the order of the buddy. The \system buddy allocator uses the same procedure for coalescing buddies as the original buddy allocator, since it depends on the physical contiguity of the two buddies. The insertion algorithm is explained in the following pseudo-code snippet.

\begin{lstlisting}
global data: list_head buddies[NR_ORDERS][NR_COLORS]

procedure INSERT_BUDDY

inputs: buddy_address addr
		buddy_order ord
		
output: none
			
code:
	order_color = PAGE_COLOR(addr) >>  ord
	
	twin_addr = addr + (1 XOR ord)	
	
	if( BUDDY_IS_FREE(twin_addr) )
		new_buddy = COALESCE_BUDDIES(addr, twin_addr, ord)
		INSERT_BUDDY(new_buddy, ord + 1)
		return
	else
		HEAD_INSERT_INTO_LIST(buddies[ord][order_color],addr)
		
end procedure
\end{lstlisting}


The removal of a buddy of order $ n $ and color $ c $ is done by checking the list of the desired color; if no buddy is present, it is necessary to split a higher order buddy. This higher order buddy must be chosen carefully as it must contain a buddy of the desired color $ c $. If the number of colors of order $ n + 1 $ is the same as that of order $ n $, then the same color list is accessed. If the number of colors is smaller, then each color of order $ n + 1 $ spans 2 colors of order $ n $: in this case the color to be accessed at order $ n + 1 $ is thus $ c/2 $, and after the splitting the proper buddy must be returned and the other stored into its own color list. Buddy splitting and removal procedures are exemplified with the following pseudo-code snippet. The main procedure is \code{REMOVE_BUDDY()}, which looks for a buddy in the global list; if a buddy of a requested color (colors are stored in a set, as an application can receive several colors) is not found, the procedure \code{SPLIT_BUDDY()} in invoked, which splits a buddy of the higher order and returns one half. Before this call, the colors set is translated into another colors set containing the colors for the higher order (different orders can have different colors, as from \autoref{subsec:buddydata_mod}).\\
The procedure \code{SPLIT_BUDDY()} recursively calls itself in two cases: in the first case when the entire order has no buddies, and in the second case after no buddy is found among those reserved to the application (through its color). In particular, if \code{SPLIT_BUDDY()} founds a suitable buddy it splits it into two halves which are represented by their initial page and computes their addresses assuming that the pages are all stored inside an array (as in Linux).

\begin{lstlisting}
global data: list_head buddies[NR_ORDERS][NR_COLORS]
			 	integer nr_colors[NR_ORDERS]


procedure MAP_COLOR_SET

inputs: buddy_color_set c_set
		buddy_order old_ord
		buddy_order new_ord
	
output: buddy_color_set

code:
	if nr_colors[new_ord] == nr_colors[old_ord]
		return c_set
	else
		buddy_color_set new_c_set = EMPTY_SET()
		shift = log2(nr_colors[old_ord] - nr_colors[new_ord])
		for color in c_set
			new_color = color >> shift
			INSERT_UNIQUE_ELEMENT(new_color,new_c_set)
			
		return new_c_set
		
end procedure


procedure SPLIT_BUDDY

inputs: buddy_color_set c_set
		buddy_color_set target_c_set
		buddy_order ord
		
output: buddy_addr
			
code:
	if ord > MAX_ORDER
		return Null

	if ORDER_HAS_NO_BUDDIES(ord)
	 	new_c_set = MAP_COLOR_SET(c_set, ord, ord + 1)
		return SPLIT_BUDDY(new_c_set, c_set, ord + 1)
		
	for color in c_set
		if buddies[ord][color] is empty
			continue
		else
			buddy = REMOVE_LIST_HEAD(buddies[ord][order_color])
			lower_half = buddy
			upper_half = buddy + (1 << ord)
			lower_color = PAGE_COLOR(lower_half) >>  (ord - 1)
			upper_color = PAGE_COLOR(upper_half) >>  (ord - 1)
			if lower_color in target_c_set
				INSERT_BUDDY(upper_buddy, ord - 1)
				return lower_buddy
			else
				INSERT_BUDDY(lower_buddy, ord - 1)
				return upper_buddy
	
	new_c_set = MAP_COLOR_SET(c_set, ord, ord + 1)
	return SPLIT_BUDDY(new_c_set, c_set, ord + 1)
	
end procedure


procedure REMOVE_BUDDY

inputs: buddy_color_set c_set
		buddy_order ord
		
output: buddy_addr
			
code:
	for color in c_set
		if c_set(color) is empty
			continue
		else
			new_c_set = hal
			return REMOVE_LIST_HEAD(buddies[ord][order_color])
	
	new_c_set = MAP_COLOR_SET(c_set, ord, ord + 1)
	return SPLIT_BUDDY(new_c_set, c_set, ord + 1)
	
end procedure

\end{lstlisting}