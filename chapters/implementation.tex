\chapter{Implementation} \label{chap:implem}
This chapter discusses the details of the implementation of \system on the basis of the design choices of the previous chapter. \autoref{sec:impl_approach} shows the way \system is implemented inside the Linux kernel, while \autoref{overall} gives an overview of the various components added with \system.\\
\autoref{color_rep} explains how \system represents page colors and uses reads the color of a memory page or of a buddy; this representation is used to associate a set of colors to an application, as \autoref{color_app} shows. Finally, \autoref{rain_buddy} discusses how the modifications to the Linux Buddy allocator are implemented.


\section{Implementation approach} \label{sec:impl_approach}
The design proposed for \system in \autoref{chap:design} is implemented on the Linux kernel version 3.9.4.

Because of the fundamental role of the physical memory allocator, this subsystem cannot be plugged or unplugged at runtime, and must be statically linked inside the Linux kernel binary. Hence, the only way to implement \system in Linux without disrupting the default codebase was to add the new source code to the kernel working tree and to switch between the default allocator and \system at compile time.
Our first care is not to change the higher levels of the Linux memory management system, in order to have the smallest possible impact on the existing infrastructure and above all on the applications running in Linux. Normal applications are therefore not isolated with respect to the \gls{LLC}, while applications wishing this feature must request it explicitly.

\section{Overall mechanism} \label{overall}
As \system is basically a modification of the Buddy allocator, it cannot be developed as a per se component, but must be tightly integrated into the Linux kernel. Several calls have been added to the Linux kernel code, whose task is explained in the next sections.\\
When the kernel boots, it is fundamental to initialize the new data structures correctly; hence a call for boot initialization is added in order to collect the cache parameters and initialize the data structures properly. The following sections explain how the data structure is initialized.

After boot, each colored application (i.e., each application whose \gls{LLC} portion is isolated) must explicitly ask the kernel for a dedicated \gls{LLC} partition through a system call \system introduces. Applications that have requested an \gls{LLC} partitions are called \emph{colored applications}.

During runtime the \emph{\system allocator} must determine which physical color to allocate from depending on the running application and on the allocation origin (userspace or kernel).
The final call performs the \emph{color reservation removal}, to make the page colors reserved to an application newly available when this application terminates.

\section{Page colors representation in \system} \label{color_rep}
Here we explain how page colors are represented and managed in \system. The data structure to do this is discussed in \autoref{subsec:cset} and how colors are read from pages and buddies is explained in \autoref{subsec:chandle}.\\
The overall objective behind the implementation we propose to represent page colors is efficiency: since the color information associated to a colored application is accessed frequently during allocation (see \autoref{sec:buddy_mod}), it is fundamental to get the assigned colors in an easy and fast way.


\subsection{Color sets representation} \label{subsec:cset}
A colored process is assigned a set of page colors, which must be represented in a compact and efficient way. The physical allocator uses this information to search a buddy of the requested file through the per-color lists (\autoref{sec:buddy_mod}). Therefore, \system needs to associate this information to the process that is colored. The data structure that contains the information of a colored process is shown in \autoref{cod:cinfo}:

\begin{lstlisting}[label=cod:cinfo,caption={Colors set representation}]
struct color_info {
	color_mask cmask;
	unsigned int last_color;
	unsigned int count;
	atomic_t thread_count;
};
\end{lstlisting}

This data structure is associated to the process it refers to via a new field of the process handler; in fact, in Linux each thread is represented by a structure \code{task_struct}, to which we added a field \code{struct color_info *cinfo} that holds the coloring information. If this field is \code{NULL} (as by default), the process is not colored, otherwise this field points to the process coloring information.

The first field \code{color_mask cmask} is a bit mask used to hold the colors associated to a process; the \system allocator must iterate over this bit mask in order to find a free buddy to allocate. This bit mask must contain at least one bit per color, where the number of colors of the architectures is indicated in the code as \code{NR_COLORS}. The third field, \code{cmask}, stores the number of colors in the bit mask.\\
The second field is a hint to decrease the number of iterations over the per-color lists: in fact, if buddy of a certain color has been accessed recently, it is very likely that its ``twin'' buddy is stored inside the next list, thanks to the splitting procedure. This may save time when the allocator must iterate upon many colors.\\
Finally, the last field contains a counter of the process threads that are running. All these threads receive physical memory from the ``color pool'' (i.e., the set of reserved colors) of the process. This counter is incremented whenever a thread is created and is decremented whenever a child process or a thread exits. The type of this counter is \code{atomic_t}: this is an architectural-independent data type defined in the kernel that guarantees the atomicity of accesses. Atomicity avoids concurrency problems (like lost updates) due to the simultaneous execution of multiple threads, which may be created or may terminate at the same time.

\system uses two global variables of type \code{struct color_info}: \code{cinfo_global} and \code{cinfo_process}. The first one contains a bit mask with all the colors in the system and is used for kernel allocations (as explained later). The second one contains a bit mask with all the colors not yet associated to any process and is used for allocating memory to non-colored processes. Whenever a process is colored, the colors assigned to the process are removed from the \code{cmask} field of \code{cinfo_process}, where they are re-set when the process terminates. This guarantees that the non-colored processes cannot allocate memory pages mapped into the \gls{LLC} partitions of colored processes, thus guaranteeing isolation.


\subsection{Reading the color} \label{subsec:chandle}
To re-insert a buddy into the data structures of the allocator, it is fundamental to read its color. This information can be derived from its address and its order. Recalling \autoref{fig:buddycol} in \autoref{subsec:buddydata_mod}, the buddy color is made by the lower bits of the buddy address. Because of the memory alignment of buddies, some the lower bits of the buddy address are 0 (depending on the buddy order). Hence, this zeroed bits are useless and the buddy color must be eventually shifted rightward to eliminate these bits. The number of positions to shift depends on the buddy order.

To find the color of the buddy, a set of C macros has been implemented. This macros are shown in \autoref{cod:cmacros}.

\begin{lstlisting}[label=cod:cmacros,caption={Macros to get page and buddy color}]
#define page_mcolor(page, order)													\
	((((unsigned long)(page_to_pfn(page))) & color_bitmask) >> (cshifts[order]))

#define page_vcolor(page) page_mcolor(page,0)

#define mcolor_from_vcolor(vcolor, order) (vcolor >> (cshifts[order] - cshifts[0]))
\end{lstlisting}

To briefly explain the meaning of these macros, throughout the code \emph{mcolor} indicates the color of a buddy of any order, while \emph{vcolor} the color of a single physical page. The first macro receives in input the physical address of a buddy and its order (parameters \code{page} and \code{order}, respectively) and computes its color. To do this, the first step is to get the physical address of the page, called \gls{PFN} in the Linux terminology. The built-in function \code{page_to_pfn()} solves this task. Then, the macro performs a bitwise AND of the \gls{PFN}  with a constant bit mask to extract the bit of the \gls{LLC} set number and finally discards the lower bits according to the buddy order. The number of the bits to be discarded for each order is stored in the array \code{cshift}, which is initialized at boot time on the basis of the \gls{L2} and \gls{L3} parameters and of the buddy order. The last macro, used throughout the allocator code, is used to find the color of a buddy from that of a page in case of higher order buddy search: it consists in a right bit shift, where the number of bits to shift is determined by the buddy order.\\

\section{Coloring an application} \label{color_app}
Applications must request \system to be ``colored'' in order to get a dedicated partition of the \gls{LLC}. The major steps to mark an application as colored are three: determining how many colors to be devoted, which colors and finally associating the set of colors to the application. \autoref{subsec:color_num} shows how to decide the number of colors to be assigned to a process, while \autoref{subsec:interface} explains the mechanism to choose which colors to assign and to associate the colors set to the requesting process.

\subsection{Determining the number of colors} \label{subsec:color_num}
Applications wishing to be colored must explicitly notify this wish to \system in order to be associated a set of page colors. How many colors each application needs to be isolated in \gls{LLC} is a careful decision which must not penalize the application nor wasting \gls{LLC} space that can be devoted to other applications. The possible penalization is due both to a limited \gls{LLC} space which increases the \gls{LLC} miss rate for the application and to the memory constraint discussed in \autoref{subsec:consequences_mem}. This is a major issue with the static partitioning policy implemented in \system, but is unavoidable with the current design; on the other side, leaving the choice of the colors number to the user allows to profile each application with a varying number of colors; this makes it possible to characterize the applications behavior with respect to the \gls{LLC}. In addition, the memory footprint of an application is a hardly predictable parameter. For these reasons, we chose to leave to the users the choice of the number of colors. This number must be communicated to \system via the interface described in the following section.

\subsection{Rainbow interface with application} \label{subsec:interface}
The interface between the application and \system is a Linux system call. This mechanism is a natural choice for this task as it is synchronous from the point of view of the application: when the application is assigned the \gls{CPU} after the system call, it is guaranteed to be colored. This is important for applications eventually relying on the benefits provided by \system.\\
Another possible implementation is adding a configuration knob to the cgroup subsystem of the Linux kernel \cite{cg}, in order to give a unified interface with other options; this choice has been discarded as it is beyond the scope of this work, because a clearer interface does not give any benefit in terms of effectiveness and a system call is an easier way to implement our prototype.
Therefore, we implemented the userspace interface of \system  as a system call which takes as input the number of colors to be allocated to the calling process. Its code is shown in \autoref{cod:syscall}.

\begin{lstlisting}[label=cod:syscall,caption={System call to color an application}]
asmlinkage long sys_use_cache_colors(const unsigned int count)
{
	unsigned long mask=0;
	unsigned int i,j;
	struct color_info *global, *ci;
	struct task_struct *p = current,*c;
	
	ci = (struct color_info*)kmalloc(sizeof(struct color_info),GFP_KERNEL);
	if(ci == NULL) {
		return -1;
	}
	for(i=0; i < count; i++) {
		mask |= 1UL << i;
	}
	spin_lock(&cinfo_lock);
	global = &cinfo_process;
	if((count > global->count) || p->cinfo != NULL) {
		goto fail;
	}
	for(i = 0; (i <= NR_COLORS - count) && (mask & reserved); i++) {
		mask <<= 1;
	}
	if(mask & reserved) {
		mask = 0;
		j=0;
		for(i = 0; (i < NR_COLORS - count) && (j < count); i++) {
			if(!color_is_set(i,&reserved)) {
				set_color(i,&mask);
				j++;
			}
		}
	}
	if(mask & reserved) {
		goto fail;
	}
	reserved |= mask;
	global->count -= count;
	global->cmask = ((1UL << NR_COLORS) - 1) &  reserved;
	spin_unlock(&cinfo_lock);
	ci->cmask = mask;
	ci->count = count;
	atomic_set(&ci->thread_count,1);
	c = p->group_leader;
	p = c;
	c->cinfo = ci;
	while_each_thread(p, c) {
		c->cinfo = ci;
		atomic_inc(&ci->thread_count);
	}
	return 0;
fail:
	spin_unlock(&cinfo_lock);
	kfree(ci);
	return -1;
}
\end{lstlisting}

The first part (lines 12-32) determines the colors to be reserved to the requesting application.
The colors are determined with a best effort mechanism. Initially the system call attempts to allocate a set of contiguous colors (lines 20-22), in order to increase the probability of buddy coalescing when freeing memory and limit external fragmentation. If this attempt fails, the call allocates non-contiguous colors throughout the memory (lines 26-32), until the specified number of colors is allocated. Then, the new configuration is stored into the variables which control the memory allocation: the allocated colors are set in a mask called \code{reserved} which stores all the colors reserved for all the colored applications (line 37); then the variable \code{cinfo_process} is modified in order to contain only the colors still available (line 38).

These operations are protected by the spinlock \code{cinfo_lock}, which is acquired at the beginning of the procedure and is released only after the changes have been saved. Even if its use would theoretically decrease the scalability of the system, this code section is not a ``hot'' one since it is invoked only in the application initialization, and contention on the spinlock is thus very unlikely.

In the final part of the call, the color reservation is stored into a variable of type \code{color_info}, which is associated to the group leader of the running process and to all its threads (lines 40-49) via the field \code{cinfo} added to the structure \code{task_struct} (that represents the processes in Linux). This choice is fundamental to confine the whole applications into the \gls{LLC} partition and prevent the other threads from mixing their data with those of other processes: Linux in fact represents every thread as a different \emph{task} via a structure of type \code{task_struct}. Therefore, the color set must be associated with  all the threads composing the application.

The final part of the call counts the number of process threads and stores it into the field \code{thread_count} atomically, for the reasons explained in \autoref{subsec:cset}.

Whenever a process calls \code{fork()} to create a child process or a new thread, its color reservation is copied from the parent process: if it is NULL, the parent process has not been colored, otherwise the child process receives the address of the same color set and the \code{thread_count} field is incremented. Instead, whenever a thread terminates via the \code{exit()} system call, this counter is decremented to keep track of only the processes effectively using the color reservation: when the counter reaches 0, the colors reservation is removed by updating the variables \code{reserved} and \code{cinfo_process} and the memory area containing the colors set is freed.

\section{Rainbow Buddy} \label{rain_buddy}
In this section we show the major modifications to the Buddy algorithm, according to the design proposed in \autoref{sec:buddy_mod}. \autoref{subsec:impl_struct} shows how the Buddy data structures have been modified, while \autoref{subsec:impl_alg} shows the changes of the algorithm itself.

\subsection{The modified Buddy structure} \label{subsec:impl_struct}
In the Linux kernel, the physical memory of a machine is divided into zones, represented by the array \code{struct zone node_zones[MAX_NR_ZONES]}. The zones are non overlapping memory areas used for different types of allocation; they are defined at compile time and depend on the architecture of the machine. For example, in a x86-64 system three zone usually exist: the \emph{DMA} zone consists in the first 16 MB of memory and is for old \gls{DMA} controllers, which use only 24 bits to store the address of the physical memory area where they copy the data read from the device; the \emph{DMA32} zone extends from 16 MB to 4 GB and is used for \gls{DMA} controllers using 32 bits for the physical address; the \emph{Normal} zone is used to control the rest of the memory. Applications are preferably given memory areas from the normal zone, and the allocator falls back to the other areas if no memory is present in this zone; similarly \gls{DMA} controllers with 32 bits address fall back to the \gls{DMA} area if no memory is left.

The structure describing a zone contains many fields, most of them used to store information; instead, the field \code{struct free_area free_area[MAX_ORDER]} is the main data structure described in \autoref{fig:buddystruct}, where the constant \code{MAX_ORDER} is the number of buddy orders allowed for the current architecture (10 by default). 

\autoref{cod:freearea} shows the re-definition of the \code{struct free_area} data structure containing all the changes described in \autoref{subsec:buddydata_mod}

\begin{lstlisting}[label=cod:freearea,caption={The new Buddy data structure}]
struct free_area {
#ifdef CONFIG_LLC_ISOLATION
	struct list_head	free_list[NR_COLORS];
	unsigned int last_color;
	unsigned int count;
#else
	struct list_head	free_list;
#endif
	unsigned long		nr_free;
};
\end{lstlisting}

where the macro \code{CONFIG_LLC_ISOLATION} enables the \system subsystem at compile time.
By comparing the two definitions of \code{free_list}, it is evident that with \system each page list is split into \code{NR_COLORS} sublists, where \code{NR_COLORS} is the number of colors in the system. This is done for all the orders, although the higher orders have a smaller number of colors for the considerations of \autoref{subsec:buddydata_mod}.

This decision simplifies the prototyping of \system : a per-order number of sublists would have required the allocation of the needed memory space at boot time in order to initialize the memory allocator itself. This chicken-egg dilemma would be solved by using the boot allocator provided by Linux, but the modifications required to implement such a feature would have required large modifications of the data structure initialization and access which are beyond the scope of this work.

In addition to the free lists, two other fields are added. The field \code{unsigned int count} counts the number of free buddies and equals the sum of all buddies listed inside the \code{free_list[NR_COLORS]} lists; this condensed information is useful when allocating memory for a non-isolated application: in order to check whether the current order and migrate type contains buddies it is sufficient to check this count without iterating through the colored lists. The field \code{unsigned int last_color} provides instead a hint for the allocation algorithm to decrease the number of iterations required to allocate a buddy: in particular, because of the splitting procedure, if a buddy is present in color $ i $ it is likely that its ``twin'' buddy is also present in color $ i + 1 $, where the algorithm will restart its search at the next allocation.\\

Related to physical page allocation, the field \code{struct per_cpu_pageset __per_cpu *pageset} inside \code{struct zone} implements an optimization of Linux for multiprocessors systems: in fact, it is defined for every \gls{CPU} of the machine (as the annotation \code{__per_cpu} suggests). This structure in turn contains a field \code{struct per_cpu_pages pcp}, which holds the most recent freed pages. Its definition, which is very similar to that of \code{free_list[NR_COLORS]}, is shown in \autoref{cod:pcparea}.

\begin{lstlisting}[label=cod:pcparea,caption={The per-cpu-pages structure}]
struct per_cpu_pages {
	int count;		/* number of pages in the list */
	int high;		/* high watermark, emptying needed */
	int batch;		/* chunk size for buddy add/remove */
#ifdef CONFIG_LLC_ISOLATION
	struct list_head lists[NR_COLORS];
	unsigned int last_color;
	unsigned int count_migtype;
#else
	struct list_head lists;
#endif
};
\end{lstlisting}

Holding the most recent freed pages is an optimization of Linux to speed up the allocation: since single pages are the most requested buddies (because user space applications are given physical pages only after a page fault event), it is useful to store a certain number of them in special per-\gls{CPU} caches, so that most allocations can be served immediately without searching in the main Buddy data structure and acquiring locks (defined for each zone) which limit scalability. The number of pages inside each cache is kept in a range defined at boot time by specific procedures, executed whenever the number of pages is inside the range: in case too few pages are present, they are reclaimed from the Buddy data structure, while in case of excess they are released into the same structure and eventually coalesced into bigger buddies. In the code listing above, we can see the same modifications introduced in the definition of \code{struct free_area}, with the same purpose.\\

The initialization of these two data structure is done at boot time and is similar to the initialization of the normal Buddy data structures: the per-\gls{CPU} pageset of the running \gls{CPU} is initially linked to a ``generic'' boot pageset, all the \gls{CPU} pagesets are later allocated at runtime and the zones free area ares initially set to 0 and then populated. Populating the free area proceeds as in the normal Buddy allocator: all the memory pages are freed one by one, added to the corresponding free area and time by time coalesced into bigger buddies; the only difference here is the per-color distribution into the different lists, shown in the following section. Similarly, the per-\gls{CPU} pagesets are initialized by claiming pages from the free area for each color.

\subsection{The modified Buddy algorithm} \label{subsec:impl_alg}
In order to explain the modifications to the Buddy algorithm, we provide here an example from one of the main functions to access the free area and the per-\gls{CPU} pagesets called \code{buffered_rmqueue}. At the beginning, it is fundamental to determine the type of allocation to be performed: this type can be isolated, non-isolated or kernel allocation. Isolated allocations are performed for processes to which a \code{struct color_info} is associated and allocate memory pages only from the reserved colors. Non-isolated applications allocate pages from non-reserved colors. Kernel allocations are served with memory pages of every color, in order to preserve the functionality of the kernel, avoid swapping of pages to disk due to high pressure on little areas of memory and allow the allocation of buffers for serving device interrupts.
The origin of the allocation is identified at the beginning of the allocation, where the per- \gls{CPU} pagesets are accessed.

\begin{lstlisting}[label=cod:aaa,caption={Determining the color set for an allocation}]
if((gfp_flags & (GFP_DMA | GFP_DMA32 | GFP_ATOMIC)) || (order > 0) || !ccount(get_allowed_cinfo())) {
	ci = get_global_cinfo();
} else if(!colored_task(current)) {
	ci = get_allowed_cinfo();
} else {
	ci = get_task_cinfo(current);
}
\end{lstlisting}

The first branch checks if a kernel allocation is happening or if all the memory colors are being used: the first condition happens if the allocation comes from the DMA subsystem or is an atomic allocation (usually coming from device drivers) or a buddy of order greater than 0 is requested (only the kernel can allocate more than a page at once). The second condition is instead given by the number (function \code{ccount()} of colors in the variable \code{cinfo_process}, accessed via its interface \code{get_allowed_cinfo()}): if it is 0, all the colors have been associated to processes and any color can be used.
The second branch checks instead whether the task currently running is \gls{LLC}-isolated: if not, the general mask for non-isolated processes is used for allocation, otherwise (last branch) the information associated to the process is used for allocation.
Going further, the code checks the order of the allocation: if it is 0, it checks the presence of pages in the per-\gls{CPU} pageset of the current core, otherwise it goes through the free areas of the Buddy allocator. In the first case it is necessary to find an allowed color for the allocation, i.e. to iterate through the set bit of the color bit mask. \autoref{cod:bbb} shows the initialization of the \code{color} variable (line 1) and the search for a non-empty per-\gls{CPU} pageset of the desired color (lines 3-5). As soon as one is found, the page is removed from the list and returned.

\begin{lstlisting}[label=cod:bbb,caption={Non-empty pageset search}]
color = find_color_round(cmask(ci),get_last_color(ci));
list = &pcp->lists[migratetype][color];
while(list_empty(list)) {
	color = find_color_round(cmask(ci),color + 1);
	list = &pcp->lists[migratetype][color];
}
\end{lstlisting}

If instead the allocation order is greater than 0, the Buddy allocator is invoked and the coloring information, stored in the variable \code{ci}, is passed to it.

Similarly, the other allocations throughout the Buddy code initially search for a free list of an allowed color which is non-empty. To take in consideration the order of the current list, the macro \code{mcolor_from_vcolor} is used as array displacement, with the fourth line of the code example above becoming

\begin{lstlisting}[label=cod:c,caption={Per-color list selection}]
list = &area->free_list[migratetype][mcolor_from_vcolor(color,current_order)];
\end{lstlisting}

The last example we provide of the modifications to the Buddy allocator is the split of a buddy into two smaller ones, performed in the function \code{expand()}. Here, the desired page color must be taken into account for the splitting: this parameter, called \code{vcolor}, is used to determine the mcolor of the buddy to be further split (or returned). \autoref{cod:c} shows a code snippet that is inserted inside a loop which iteratively decrements the buddy order (variable \code{high}) and splits the buddy stored into the variable \code{page}.

\begin{lstlisting}[label=cod:d,caption={Splitting of a free buddy}]
twin = &page[size];
if(mcolor_from_vcolor(vcolor,high) >= page_mcolor(twin,high) && high < single_list_min_index) {
	tmp = twin;
	twin = page;
	page = tmp;
}
list_add(&pag->lru, &area->free_list[migratetype][page_mcolor(twin,high)]);
\end{lstlisting}

In the first line the variable \code{twin} is initialized to the first page of the second half of the buddy to be split, while the passed parameter \code{page} contains the first page (recalling that the pages structures are stored inside a unique array). The branch checks whether \code{twin} contains the buddy of the desired mcolor and if the current buddy order spans more than one mcolor: in this case the variables \code{twin} and \code{page} are swapped for \code{page} to finally point to the buddy to be split again or to be returned. At the end, the \code{twin} buddy is stored into the free list.