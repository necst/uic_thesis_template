# UIC master thesis template

Based on https://github.com/mikispag/UICThesis-Template, in turn based on https://www.math.uic.edu/graduate/current/uicthesi.
The main addition is the support of url fields inside bibliographical entries. See ``thesis.bib`` for examples.

The UIC thesis style is based on a custom template and on custom bibliography parsing. Although some options are available, deep modifications to the style may be quite hard as they would need changing the ``uicthesis.cls`` file, such as modifications to the bibliography parsing would require dealing with the parsing code inside ``bibstyle.bst``. However, they are unlikely to be needed.

**Note that this thesis is from 2014, so it is up to you to check that the format guidelines have not changed in the while**. Although this is unlikely, check https://www.math.uic.edu/graduate/current/uicthesi from time to time; and, please, **push here to share the changes**.

Requirements:
* pdflatex
* bibtex
* makeglossaries

To compile run ``make``, to clean intermediate files run ``make clean``and to clean everything ``make distclean``.
