main := thesis

.PHONY: clean distclean

LTC = pdflatex -interaction=nonstopmode -shell-escape -synctex=1
BIB = bibtex
GLO = makeglossaries

all: $(main).tex $(main).bib
	$(LTC) $(main).tex
	$(BIB) $(main)
	$(GLO) $(main)
	$(LTC) $(main).tex
	$(GLO) $(main)
	$(LTC) $(main).tex

clean:
	@rm *.bak $(main).aux $(main).bbl $(main).blg $(main).dvi $(main).log $(main).out $(main).pdf $(main).ps $(main).glo $(main).gls $(main).lot $(main).lof $(main).toc $(main).xdy $(main).glg $(main).synctex.gz **/*.aux &> /dev/null || true

distclean: clean
	@rm $(main).pdf &> /dev/null || true

